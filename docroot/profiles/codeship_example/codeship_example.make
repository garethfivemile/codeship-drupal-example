api = 2
core = 7.x
# API & Core version

; Core project
; ------------
projects[drupal][version] = 7.39

; Core patch: Add .ht.router.php
; https://www.drupal.org/node/1543858#comment-9704283
projects[drupal][patch][] = https://www.drupal.org/files/issues/add_a_startup-1543858-30.patch

; Contrib projects
; ----------------
projects[workbench][version] = 1.2
projects[workbench][subdir] = contrib

projects[views][version] = 3.11
projects[views][subdir] = contrib

projects[ctools][version] = 1.7
projects[ctools][subdir] = contrib

Feature: View my workbench
  As an administrator
  I want to view my workbench
  So that I can get an overview on recent content
@api
  Scenario: Visit the 'My workbench' page
    Given I am logged in as a user with the "administrator" role
    Given I am on "/admin/workbench"
    Then I should see text matching "All Recent Content"
####################################
Example for using Codeship + Drupal.
####################################

This is a simple install profile for demonstrating testing a drupal site
on codeship.com.

The install profile is based on the Drupal 'Standard' profile, but with
added dependencies for Workbench, Views & Ctools.

The automated test will check that an administrator user can view the
'My Workbench' page once the profile is installed.

-----------------
Project structure
-----------------
The git repository has the following folders:
- docroot
- tests

Docroot is where Drupal should be. In this example, the .make file is used
to build the project & add the Drupal files. Tests contains the Behat features.

-----------------
Codeship settings
-----------------

To configure codeship to setup the installation & run the tests, you will
need the following:

Test settings - build Drupal & install the profile:
---------------------------------------------------
# Set php version through phpenv. 5.3, 5.4 and 5.5 available
phpenv local 5.4
# Install drush
composer global require drush/drush:6.* --prefer-source --no-interaction
export PATH="$HOME/.composer/vendor/bin:$PATH"
# Set up Drupal
cd docroot
# Build the make file
drush make --yes --contrib-destination=profiles/codeship_example/ profiles/codeship_example/codeship_example.make
# Install Drupal
drush site-install codeship_example install_configure_form.update_status_module='array(FALSE,FALSE)' --verbose --account-mail=dev@fivemilemedia.co.uk --db-url=mysql://$MYSQL_USER:$MYSQL_PASSWORD@localhost/test --site-mail=dev@fivemilemedia.co.uk --account-pass=pass -y
# https://www.drupal.org/node/1543858
# Start PHP server
nohup bash -c "php -S 127.0.0.1:8000 .ht.router.php 2>&1 &" && sleep 1; cat nohup.out
# setup selenium
cd ../tests
# Copy behat settings file
cp codeship.behat.yml behat.yml

Test commands - run behat tests:
--------------------------------
# Changed to tests directory at the end of the setup commands, so just run Behat.
bin/behat